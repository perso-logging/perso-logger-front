FROM nginx:1.23

ENV JSFOLDER=/usr/share/nginx/html/assets/*.js

COPY dist /usr/share/nginx/html

COPY ./start-nginx.sh /usr/bin/start-nginx.sh
RUN chmod +x /usr/bin/start-nginx.sh

WORKDIR /usr/share/nginx/html

ENTRYPOINT [ "start-nginx.sh" ]