import React from 'react'
import { createBrowserRouter } from 'react-router-dom'
import Dashboard from '../pages/Dashboard'

const routes = createBrowserRouter([
	{
		element: <Dashboard title="PersoLogger dashboard" />,
		path: '/'
	}
])

export default routes