import { $fetch } from 'ohmyfetch'
console.log(import.meta.env.VITE_BACKEND_BASE_URL)
const fetchToBackend = $fetch.create({
	baseURL: import.meta.env.VITE_BACKEND_BASE_URL
})

export default fetchToBackend