export const week = [
	'monday',
	'tuesday',
	'wednesday',
	'thursday',
	'friday',
	'saturday',
	'sunday'
] as const

/**
 * convert minutes into a Tuple of hour - minutes
 * @param {number} minutes 
 * @return {[number, number]} - [hours, minutes]
 */
export function convertMinutesToHours(minutes: number): [number, number] {
	return [Math.floor(minutes / 60), minutes % 60]
}