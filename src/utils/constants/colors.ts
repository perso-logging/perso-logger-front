export default {
	'yellow-light': '#FFCD9E',
	'green-light': '#19BA1F',
	'gray-light': '#D9D9D9',
	'green-valid': '#7FCB8B'
}