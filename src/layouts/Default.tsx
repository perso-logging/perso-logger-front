import React, {PropsWithChildren} from 'react'
import Header from '../components/Header'

interface defaultLayoutProps extends PropsWithChildren {
    headerActions?: JSX.Element
}

export default function Defaultlayout(props: defaultLayoutProps) {


	return (
		<div className="flex flex-col min-h-screen bg-yellow-light">
			<Header
				actions={props.headerActions}
			/>
			{props.children}
		</div>
	)
}