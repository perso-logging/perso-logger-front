import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import './index.scss'
import { createTheme, ThemeProvider } from '@mui/material'
import colors from './utils/constants/colors'

const theme = createTheme({
	palette: {
		...Object.entries(colors).reduce((obj, [k,v]) => Object.assign(obj, {[k]: {main: v}}),{})
	},
})
ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
	<React.StrictMode>
		<ThemeProvider theme={theme}>
			<App />
		</ThemeProvider>
	</React.StrictMode>
)
