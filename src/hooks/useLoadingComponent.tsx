import React from 'react'
import { Card } from 'antd' 


export default function useLoadingComponent (count = 1, loader?: JSX.Element) {

	const components: JSX.Element[] = []

	for(let i = 0; i< count; i++) {
		components.push(
			loader ?? <Card style={{ width: 300, marginTop: 16 }} loading key={'loader' + i} />
		)
	}

	return {
		components
	}
}