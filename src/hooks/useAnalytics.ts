import { useState } from 'react'
import fetchToBackend from '../utils/fetchToBackend'
import { AllAnalytics } from './../../types/backend/index'


export default function useAnalytics() {
	const [loading, setLoading] = useState(false)

	const [list, setList] = useState<AllAnalytics>([])

	async function getAllAnalytics(): Promise<AllAnalytics> {
		return await fetchToBackend<AllAnalytics>('/analytics')
	}

	/**
     * reload analytics data with loading state
     */
	async function reloadAnalytics() {
		setLoading(true)
		try {
			setList(await getAllAnalytics())
		} finally {
			setLoading(false)
		}
	}

    function findAnalytic(name: string) {
        return list.find((a) => a.projectName === name)
    }

	return {
		loading,
		getAllAnalytics,
		list,
        findAnalytic,
		reloadAnalytics
	}
}