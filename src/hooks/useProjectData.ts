import { ProjectData } from './../../types/backend/index'
import { millisecondsToMinutes  } from 'date-fns'
import{format, utcToZonedTime} from "date-fns-tz"
import { useState, useEffect } from 'react'
import { ProjectGetAllData } from './../../types/backend'
import fetchToBackend from '../utils/fetchToBackend'
import {useRef} from 'react'
import { convertMinutesToHours } from '../utils/time'
interface Opts {
	timeElapsed: boolean
}

export default function useProjectData(opts: Partial<Opts> = {}) {

	const {timeElapsed = false} = opts

	const [loading, setLoading] = useState(false)

	const [list, _setList] = useState<ProjectGetAllData>([])
	const listRef = useRef<ProjectGetAllData>([])

	function setList(list: ProjectGetAllData) {
		_setList(list)
		listRef.current = list
	}

	function projectHasLastTimer(p: ProjectData) {
		return p.lastTimer && p.lastTimer !== '-'
	}

	async function getAll(): Promise<ProjectGetAllData> {
		return await fetchToBackend<ProjectGetAllData>('/list')
	}

	async function newProject(name: string) {
		return await fetchToBackend('/new', {
			method: 'POST',
			body: JSON.stringify({ name })
		})
	}

	async function pauseOne(name: string) {
		return await fetchToBackend('/stop', {
			method: 'PUT',
			body: JSON.stringify({ name })
		})
	}

	async function deleteOne(name: string) {
		return await fetchToBackend('/remove', {
			method: 'DELETE',
			body: JSON.stringify({ name })
		})
	}

	async function startOne(name: string) {
		return await fetchToBackend('/start', {
			method: 'PUT',
			body: JSON.stringify({ name })
		})
	}

	async function getAllSummary() {
		return await fetchToBackend('/summary')
	}

	function getProjectTimeSpent(project: ProjectData): string {
		const utc = utcToZonedTime(Date.now(), 'Europe/Paris')
		const currentTime = utc.getTime()
		if(project.lastTimer) {
			const zoned = utcToZonedTime(project.lastTimer+'Z', 'Europe/Paris')
			const distanceInMillisecond = currentTime - zoned.getTime()
			const minutes = millisecondsToMinutes(distanceInMillisecond)
			const time = convertMinutesToHours(minutes)
			return `${time[0]} hours and ${time[1]} minutes`
		}

		return '0 seconds'
	}

	async function reloadAll() {
		setLoading(true)
		try {
			setList(await (await getAll()).map((p) => {
				return projectHasLastTimer(p) ? {...p, timeElapsed: getProjectTimeSpent(p)} : p
			}))
		} finally {
			setLoading(false)
		}
	}

	if (timeElapsed) {
		useEffect(() => {
			const watcher = setInterval(() => {
				setList(listRef.current.map((p) => {
					return projectHasLastTimer(p) ? {...p, timeElapsed: getProjectTimeSpent(p)} : p
				}))
			}, 1000)

			return () => { clearInterval(watcher) }
		}, [])
	}

	return {
		getAll,
		pauseOne,
		deleteOne,
		startOne,
		getAllSummary,
		list,
		loading,
		reloadAll,
		setLoading,
		newProject
	}
}