import React, { useEffect, useState } from 'react'
import useProjectData from '../hooks/useProjectData'
import Defaultlayout from '../layouts/Default'
import {  Card, Input ,Skeleton, Modal} from 'antd'
import {Button, IconButton} from '@mui/material'
import useLoadingComponent from '../hooks/useLoadingComponent'
import { CaretRightOutlined, PauseOutlined, DeleteFilled, InteractionFilled, FileAddFilled} from '@ant-design/icons'
import useAnalytics from '../hooks/useAnalytics'
import { convertMinutesToHours } from '../utils/time'
import MoleculeWeekAnalytic from '../components/Molecules/MoleculeWeekAnalytic'
import MoleculeInput from '../components/Molecules/MoleculeInput'
  
export default function Dashboard(props: {title: string}) {
	useEffect(() => {document.title = props.title}, [])
	
	const { newProject, pauseOne, startOne, deleteOne, list: projects, reloadAll: reloadProjects, loading: projectLoading, setLoading: setProjectLoading} = useProjectData({timeElapsed: true})
	const { reloadAnalytics, loading: analyticsLoading, findAnalytic}  = useAnalytics()
 
	const {components} = useLoadingComponent(3)
	const [showModalNew, setModalNew] = useState(false)

	const showModal = () => {
		setModalNew(true)
	}

	const [deleteModalId, setDeleteModalId] = useState<null|string>(null)
	
	async function withLoading(callback: () => any) {
		setProjectLoading(true)
		try {
			return await callback()
		}finally {
			setProjectLoading(false)
		}
	}

	useEffect(() => {
		reloadProjects()
		reloadAnalytics()
	}, [])
 
	function displayProjects  ()  {
		return projects.map((project, index) => {
			const analytic = findAnalytic(project.name)
			const currentWeekTime = analytic ? convertMinutesToHours(analytic.totalCurrentWeek) : 0
			const lastWeekTime = analytic ? convertMinutesToHours(analytic.totalLastWeek) : 0

			return <Card key={project.name+index} className={project.timerState === 'ACTIVE' ? 'bg-green-valid' : 'bg-gray-light'}>
				<div className="grid grid-cols-2 justify-between p-5 gap-10">
					<div className="text-3xl font-bold gap-5 flex flex-col border-r-2 border-black px-4">
						<p>{project.name}</p>
						
						<p>Spending time: { project.timeElapsed ?? '0 seconds'}</p>

						<div className="actions justify-between text-8xl">
							<div className="flex gap-10">
								{project.timerState === 'ACTIVE' 
									?<IconButton  
										size='large'
										onClick={() => withLoading(async () => {await pauseOne(project.name); reloadProjects()})}
										title={`Pause ${project.name}`}
										aria-label="Pause timer">
										<PauseOutlined className="text-yellow-400" />
									</IconButton>
									: <IconButton  
										aria-label="Start timer"
										size='large'
										title={`Start ${project.name}`}
										onClick={() => withLoading(async() => {await startOne(project.name); reloadProjects()})}>
										<CaretRightOutlined className="text-green-light" />
									</IconButton>}
								<IconButton
									size='large'
									aria-label="Reset timer" title={`Reset ${project.name}`}>
									<InteractionFilled className="text-yellow-600" />
								</IconButton>
							</div>
							<IconButton  
								size='large'
								aria-label="Delete task"
								title={`Delete ${project.name}`}
								onClick={() => setDeleteModalId(project.name)}>
								<DeleteFilled className=" text-red-700" />
							</IconButton>
						</div>
					</div>
					<div className="flex flex-1 flex-col justify-between">
						<p>This week: {currentWeekTime ? `${currentWeekTime[0]}h ${currentWeekTime[1]}mn` : '--'}</p>
						<p>Last week: {lastWeekTime ?  `${lastWeekTime[0]}h ${lastWeekTime[1]}mn` : '--'}</p>
						{
							analyticsLoading 
								? <Skeleton /> 
								:  analytic ? <MoleculeWeekAnalytic weekDetails={analytic.currentWeekDetails} /> : ''
						}
					</div>
					
				</div>
			</Card>
		})
	}


 
	function createProjectModal () {
		const [name, setName] = useState('')

		const [loading, setLoading] = useState(false)

		const [errorText, setError] = useState(null)

		async function createProject(name: string) {
			setLoading(true)
			try {
				await newProject(name)
				setModalNew(false)
				reloadProjects()
			} finally {
				setLoading(false)
			}
		}

		const footer = <div className='actions '>
			<Button
				variant="contained" 
				color="error"
				onClick={() => setModalNew(false)}>
					Cancel
			</Button>
			<Button
				variant="contained" 
				color='success'
				className="bg-green-light h-fit w-fit my-auto text-white"
				onClick={() => createProject(name)}>
					Submit
			</Button>
		</div>
		
		return (
			<Modal
				confirmLoading={loading}
				open={showModalNew}
				title="New project"
				okText="Submit"
				footer={footer}
				onCancel={() => setModalNew(false)}
			>
				<div>
					<MoleculeInput label="Project name" value={name} onChange={(e) => setName(e.target.value)} />
				</div>
			</Modal>
		)
	}

	return ( 
		<Defaultlayout
			headerActions={
				<Button 
					variant="contained"
					className="bg-green-light h-fit w-fit my-auto text-white"
					onClick={showModal}
					color='success'
				>
					<FileAddFilled className="mr-1" /> New Project
				</Button>
			}
		>			
			<div className="container mx-auto flex flex-col gap-10">
				{createProjectModal()}
				<Modal 
					open={deleteModalId !== null} 
					onCancel={() => setDeleteModalId(null)}
					onOk={() => withLoading(async() => {await deleteOne(deleteModalId as string); setDeleteModalId(null); reloadProjects()})}
				>
					Do you really wish to delete {deleteModalId} ? This is irreversible.
				</Modal>
				{projectLoading ? components : displayProjects()}
			</div>
		</Defaultlayout> 
	)
}