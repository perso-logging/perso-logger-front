import React from 'react'
import routes from './router'
import {RouterProvider} from 'react-router-dom'

function App() { 
	return (
		<React.StrictMode>
			<RouterProvider router={routes} />
		</React.StrictMode>
	)
}

export default App
