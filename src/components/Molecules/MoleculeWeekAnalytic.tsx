import React from 'react'
import type {Analytic} from '../../../types/backend/index' 
import { week } from '../../utils/time'
import AtomDayAnalytic from '../atom/AtomDayAnalytic'

interface WeekAnalyticProps {
    weekDetails: Analytic['currentWeekDetails'] 
}

export default function MoleculeWeekAnalytic (props: WeekAnalyticProps) {

	function renderAnalytic() {
		return week.map((day, index) => <AtomDayAnalytic key={day + index} day={day} time={props.weekDetails[day]} />)
	}

	return (
		<div className="molecule-week-analytics flex justify-between gap-8 flex-wrap">
			{renderAnalytic()}  
		</div>
	)
}