import React from 'react'
import {Input, InputProps} from 'antd'

interface MoleculeInputProps extends InputProps {
    label: string
}

export default function MoleculeInput(props: MoleculeInputProps) {


	return (
		<div className="molecule-input">
			<label>{props.label}</label>
			<Input {...props } />
		</div>
	)
}
