import React from 'react'
import {convertMinutesToHours, week} from './../../utils/time'

interface DayAnalyticProps {
    day: typeof week[number],
    time: number
}

export default function AtomDayAnalytic(props: DayAnalyticProps) {

	const time = props.time ? convertMinutesToHours(props.time) : 0

	return (
		<div>
			<p>{props.day}</p>
			<p>{time ? `${time[0]}h ${time[1]}mn` : '--'}</p>
		</div>
	)
}