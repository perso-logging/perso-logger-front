import React from 'react'
import Logo from '../assets/perso-logger-logo.svg'

interface HeaderProps extends React.PropsWithChildren {
    actions?: JSX.Element
}

export default function Header(props: HeaderProps) {


	return (
		<header className="flex justify-between">
			<div className="logo-wrapper p-5">
				<img src={Logo} alt="PersoLogger logo" style={{height: 40}} />
			</div>

			<div className="actions flex gap-4 mr-3">
				{props.actions}
			</div>
		</header>
	)
}