#!/usr/bin/env bash

for file in $JSFOLDER;
do
  cat $file | sed "s~http://localhost:8080/~$API_URL~g" > "${file}_temp"
  mv "${file}_temp" $file
done
nginx -g 'daemon off;'