
export interface ProjectData {
    name: string,
    timerState: 'ACTIVE' | 'STOPPED',
    totalTime: number,
    lastTimer: string,
    timeElapsed? : string
}


// POST to /new
export interface ProjectCreateBody {
    name: string
}

// DELETE to /remove
export interface ProjectDeleteBody {
    name: string
}

// GET to /list
export type ProjectGetAllData = ProjectData[]

// PUT body data to /start
export interface TimerStartBody {
    name: string
}

// PUT body to /stop
export interface TimerStopBody {
    name: string
}

export interface Analytic {
    projectName: string,
    totalCurrentWeek: number,
    totalLastWeek: number,
    currentWeekDetails: {
        monday: number,
        tuesday: number,
        wednesday: number,
        thursday: number,
        friday: number,
        saturday: number,
        sunday: number
    }
}

// GET to /analytics
export type AllAnalytics = Array<Analytic>