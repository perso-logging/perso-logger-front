const { ModuleGraph } = require("vite") 

module.exports = {
	content: [
		'./src/**/*.{ts,tsx}'
	],
	theme: {
		extend: {
			colors: {
				'yellow-light': '#FFCD9E',
				'green-light': '#19BA1F',
				'gray-light': '#D9D9D9',
				'green-valid': '#7FCB8B'
			}
		},
	},
	plugins: [],
	corePlugins: {
		preflight: false
	}
}
