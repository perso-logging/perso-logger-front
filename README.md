# Frontend of the app PersoLogger


## How to setup

The project use npm as a build system.

In order to run the frontend locally:

```shell
npm install
npm run dev
```

If you want create a production image, just run

```shell
npm run build
```

And create the image with the Dockerfile.
